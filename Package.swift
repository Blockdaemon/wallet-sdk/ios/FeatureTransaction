// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "FeatureTransaction",
    platforms: [
        .iOS(.v16),
        .macOS(.v13)
    ],
    products: [
        .library(
            name: "FeatureTransaction",
            targets: ["FeatureTransactionData", "FeatureTransactionDomain"]),
        .library(
            name: "FeatureTransactionData",
            targets: ["FeatureTransactionData"]
        ),
        .library(
            name: "FeatureTransactionDomain",
            targets: ["FeatureTransactionDomain"]
        )
    ],
    dependencies: [
        .package(
            url: "https://github.com/Liftric/DIKit.git",
            exact: "1.6.1"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/CommonCryptoKit",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/Extensions",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/NetworkKit",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/Errors",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/ThresholdSecurity",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/ToolKit",
            exact: "1.0.0"
        )
    ],
    targets: [
        .target(
            name: "FeatureTransactionDomain",
            dependencies: [
                "NetworkKit",
                "Extensions",
                "CommonCryptoKit",
                "Errors",
                "ToolKit",
                .product(name: "DIKit", package: "DIKit")
            ]
        ),
        .target(
            name: "FeatureTransactionData",
            dependencies: [
                .target(name: "FeatureTransactionDomain"),
                .product(name: "DIKit", package: "DIKit"),
                "NetworkKit",
                "Extensions",
                "CommonCryptoKit",
                "Errors",
                "ToolKit",
                "ThresholdSecurity"
            ]
        ),
        .testTarget(
            name: "FeatureTransactionTests",
            dependencies: [
                .target(name: "FeatureTransactionDomain"),
                .target(name: "FeatureTransactionData"),
                "Errors"
            ]
        )
    ]
)
