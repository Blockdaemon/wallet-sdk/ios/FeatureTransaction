// Copyright © Blockdaemon All rights reserved.

import CommonCryptoKit
import Foundation

public struct UnsignedTransaction<Intent: Decodable> {
    public let unsignedTransactionHash: String
    public let intent: Intent
    public let signingInputs: [SigningInput]
    
    public init(
        unsignedTransactionHash: String,
        intent: Intent,
        signingInputs: [SigningInput]
    ) {
        self.unsignedTransactionHash = unsignedTransactionHash
        self.intent = intent
        self.signingInputs = signingInputs
    }
}
