// Copyright © Blockdaemon All rights reserved.

import DIKit

extension DependencyContainer {
    
    // MARK: - FeatureTransactionDomain
    
    public static var featureTransactionDomain = module {
        
        factory { TransactionService() as TransactionServiceAPI }
    }
}
