// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import Errors
import Foundation
import ToolKit

public enum TransactionServiceError: Error {
    case repository(TransactionRepositoryError)
}

public protocol TransactionServiceAPI {
    
    func createUnsignedTransactionForUserId<E: Encodable, D: Decodable>(
        _ userId: String,
        inputs: E,
        supportedProtocol: Data
    ) -> AnyPublisher<UnsignedTransaction<D>, TransactionServiceError>
    
    func signUnsignedTransaction<Intent: Decodable>(
        _ unsignedTransaction: UnsignedTransaction<Intent>,
        userId: String,
        supportedProtocol: Data
    ) -> AnyPublisher<String, TransactionServiceError>
    
    func submitSignedTransaction(
        _ transaction: String,
        protocolId: String,
        userId: String
    ) -> AnyPublisher<String, TransactionServiceError>
}

final class TransactionService: TransactionServiceAPI {
    
    private let repository: TransactionRepositoryAPI
    
    init(repository: TransactionRepositoryAPI = resolve()) {
        self.repository = repository
    }

    func createUnsignedTransactionForUserId<E: Encodable, D: Decodable>(
        _ userId: String,
        inputs: E,
        supportedProtocol: Data
    ) -> AnyPublisher<UnsignedTransaction<D>, TransactionServiceError> {
        repository
            .createUnsignedTransactionForUserId(
                userId,
                inputs: inputs,
                supportedProtocol: supportedProtocol
            )
            .mapError(TransactionServiceError.repository)
            .eraseToAnyPublisher()
    }
    
    func signUnsignedTransaction<Intent: Decodable>(
        _ unsignedTransaction: UnsignedTransaction<Intent>,
        userId: String,
        supportedProtocol: Data
    ) -> AnyPublisher<String, TransactionServiceError> {
        repository
            .signTransactionHash(
                unsignedTransaction.unsignedTransactionHash,
                signingInputs: unsignedTransaction.signingInputs,
                userId: userId,
                supportedProtocol: supportedProtocol
            )
            .mapError(TransactionServiceError.repository)
            .eraseToAnyPublisher()
    }
    
    func submitSignedTransaction(
        _ transaction: String,
        protocolId: String,
        userId: String
    ) -> AnyPublisher<String, TransactionServiceError> {
        repository
            .submitSignedTransaction(
                transaction,
                protocolId: protocolId,
                userId: userId
            )
            .mapError(TransactionServiceError.repository)
            .eraseToAnyPublisher()
    }
}
