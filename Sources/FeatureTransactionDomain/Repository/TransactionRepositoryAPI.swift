// Copyright © Blockdaemon All rights reserved.

import Combine
import CommonCryptoKit
import Errors
import Foundation

public enum TransactionRepositoryError: Error {
    case unsupportedProtocolDefinition(Data, Error)
    case network(NetworkError)
    case tsm(Error)
}

public protocol TransactionRepositoryAPI {
    func createUnsignedTransactionForUserId<E: Encodable, D: Decodable>(
        _ userId: String,
        inputs: E,
        supportedProtocol: Data
    ) -> AnyPublisher<UnsignedTransaction<D>, TransactionRepositoryError>
    
    func signTransactionHash(
        _ transactionHash: String,
        signingInputs: [SigningInput],
        userId: String,
        supportedProtocol: Data
    ) -> AnyPublisher<String, TransactionRepositoryError>
    
    func submitSignedTransaction(
        _ transactionHash: String,
        protocolId: String,
        userId: String
    ) -> AnyPublisher<String, TransactionRepositoryError>
}
