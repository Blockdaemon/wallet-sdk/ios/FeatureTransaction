// Copyright © Blockdaemon All rights reserved.

import DIKit
import FeatureTransactionDomain

extension DependencyContainer {
    
    // MARK: - FeatureTransactionData
    
    public static var featureTransactionData = module {
        
        factory { APIClient() as TransactionClientAPI }
        
        factory { TransactionRepository() as TransactionRepositoryAPI }
    }
}
