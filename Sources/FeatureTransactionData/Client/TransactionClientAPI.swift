// Copyright © Blockdaemon All rights reserved.

import Combine
import Errors
import Foundation

protocol TransactionClientAPI {
    func createUnsignedTransactionWithRequest<E: Encodable, D: Decodable>(
        _ request: UnsignedTransactionRequest<E>,
        userId: String
    ) -> AnyPublisher<UnsignedTransactionResponse<D>, NetworkError>
    
    func generateSignedTransactionWithRequest(
        _ signedTransactionRequest: SignedTransactionRequest,
        userId: String
    ) -> AnyPublisher<String, NetworkError>
    
    func submitSignedTransaction(
        _ transaction: SignedTransaction,
        userId: String
    ) -> AnyPublisher<String, NetworkError>
}

