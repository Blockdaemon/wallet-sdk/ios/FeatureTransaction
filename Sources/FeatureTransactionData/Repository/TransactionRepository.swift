// Copyright © Blockdaemon All rights reserved.

import Combine
import CommonCryptoKit
import DIKit
import Errors
import Extensions
import FeatureTransactionDomain
import Foundation
import ThresholdSecurityDomain
import ToolKit

final class TransactionRepository: TransactionRepositoryAPI {
    
    private let client: TransactionClientAPI
    private let keyIdDirectory: KeyIdDirectoryAPI = resolve()
    private let signing: MessageSigningServiceAPI = resolve()
    
    init(client: TransactionClientAPI = resolve()) {
        self.client = client
    }
    
    func createUnsignedTransactionForUserId<E: Encodable, D: Decodable>(
        _ userId: String,
        inputs: E,
        supportedProtocol: Data
    ) -> AnyPublisher<UnsignedTransaction<D>, TransactionRepositoryError> {
        var metadata: ChainMetadata?
        do {
            metadata = try JSONDecoder().decode(ChainMetadata.self, from: supportedProtocol)
        } catch {
            return .failure(.unsupportedProtocolDefinition(supportedProtocol, error))
        }
        guard let metadata = metadata else { impossible() }
        guard let keyId = keyIdDirectory[userId, metadata.curveName]?.identifier else { fatalError() }
        Logger.shared.debug("KeyID: \(keyId)")
        return client
            .createUnsignedTransactionWithRequest(
                UnsignedTransactionRequest<E>.init(protocolId: metadata.id, keyId: keyId, inputs: inputs),
                userId: userId
            )
            .mapError(TransactionRepositoryError.network)
            .map { response in
                UnsignedTransaction(
                    unsignedTransactionHash: response.unsignedTransactionHash, 
                    intent: response.intent,
                    signingInputs: response.signingInputs.map { .init(index: $0.index, value: $0.value) }
                )
            }
            .eraseToAnyPublisher()
    }
    
    func submitSignedTransaction(
        _ transactionHash: String,
        protocolId: String,
        userId: String
    ) -> AnyPublisher<String, TransactionRepositoryError> {
        client
            .submitSignedTransaction(
                .init(
                    signedTransaction: transactionHash,
                    protocolId: protocolId
                ),
                userId: userId
            )
            .mapError(TransactionRepositoryError.network)
            .eraseToAnyPublisher()
    }
    
    func signTransactionHash(
        _ transactionHash: String,
        signingInputs: [SigningInput],
        userId: String,
        supportedProtocol: Data
    ) -> AnyPublisher<String, TransactionRepositoryError> {
        var metadata: ChainMetadata?
        do {
            metadata = try JSONDecoder().decode(ChainMetadata.self, from: supportedProtocol)
        } catch {
            return .failure(.unsupportedProtocolDefinition(supportedProtocol, error))
        }
        guard let metadata = metadata else { impossible() }
        guard let keyId = keyIdDirectory[userId, metadata.curveName]?.identifier else { fatalError() }
        Logger.shared.debug("KeyID: \(keyId)")
        return signing
            .generateSigningInputsFromTransactionHash(
                transactionHash,
                userId: userId,
                signingInputs: signingInputs,
                keyId: keyId,
                chainMetadata: metadata
            )
            .mapError(TransactionRepositoryError.tsm)
            .flatMap { signedInputs in
                self.generateSignedTransactionWithTransactionHash(
                    transactionHash,
                    signedInputs: signedInputs,
                    keyId: keyId,
                    protocolId: metadata.id,
                    userId: userId
                )
            }
            .eraseToAnyPublisher()
    }
    
    private func generateSignedTransactionWithTransactionHash(
        _ transactionHash: String,
        signedInputs: [SignedInputs],
        keyId: String,
        protocolId: String,
        userId: String
    ) -> AnyPublisher<String, TransactionRepositoryError> {
        client
            .generateSignedTransactionWithRequest(
                .init(
                    unsignedTransaction: transactionHash,
                    signedInputs: signedInputs,
                    keyId: keyId,
                    protocolId: protocolId
                ),
                userId: userId
            )
            .mapError(TransactionRepositoryError.network)
            .eraseToAnyPublisher()
    }
}
