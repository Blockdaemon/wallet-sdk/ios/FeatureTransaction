// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import Errors
import Foundation
import NetworkKit

final class APIClient: TransactionClientAPI {
    
    private enum Path: String {
        
        case unsignedTransactions = "unsigned-transactions"
        case submitSignedTransaction = "submit-signed-transactions"
        case generateSignedTransaction = "generate-signed-transactions"
        
        static func makePathWithUserId(
            _ userId: String,
            path: Path
        ) -> [String] {
            ["waas", "v1", "application-users", userId, "wallet-operations", path.rawValue]
        }
    }
    
    private enum Parameter { }
    
    private let networkAdapter: NetworkAdapterAPI
    private let requestBuilder: RequestBuilder
    
    // MARK: - Init
    
    init(
        requestBuilder: RequestBuilder = resolve(),
        networkAdapter: NetworkAdapterAPI = resolve()
    ) {
        self.requestBuilder = requestBuilder
        self.networkAdapter = networkAdapter
    }
    
    func createUnsignedTransactionWithRequest<E: Encodable, D: Decodable>(
        _ request: UnsignedTransactionRequest<E>,
        userId: String
    ) -> AnyPublisher<UnsignedTransactionResponse<D>, NetworkError> {
        
        let request = requestBuilder.post(
            path: Path.makePathWithUserId(userId, path: .unsignedTransactions),
            body: try? JSONEncoder().encode(request),
            headers: [HttpHeaderField.accept: HttpHeaderValue.json],
            authenticated: true
        )!
        
        return networkAdapter
            .perform(request: request)
            .eraseToAnyPublisher()
    }
    
    func generateSignedTransactionWithRequest(
        _ signedTransactionRequest: SignedTransactionRequest,
        userId: String
    ) -> AnyPublisher<String, NetworkError> {
        
        struct Response: Decodable {
            let signedTransaction: String
        }
        
        let request = requestBuilder.post(
            path: Path.makePathWithUserId(userId, path: .generateSignedTransaction),
            body: try? JSONEncoder().encode(signedTransactionRequest),
            headers: [HttpHeaderField.accept: HttpHeaderValue.json],
            authenticated: true
        )!
        
        return networkAdapter
            .perform(request: request, responseType: Response.self)
            .map(\.signedTransaction)
            .eraseToAnyPublisher()
    }
    
    func submitSignedTransaction(
        _ signedTransaction: SignedTransaction,
        userId: String
    ) -> AnyPublisher<String, NetworkError> {
        
        struct Response: Decodable {
            let id: String
        }
        
        let request = requestBuilder.post(
            path: Path.makePathWithUserId(userId, path: .submitSignedTransaction),
            body: try? JSONEncoder().encode(signedTransaction),
            headers: [HttpHeaderField.accept: HttpHeaderValue.json],
            authenticated: true
        )!
        
        return networkAdapter
            .perform(request: request, responseType: Response.self)
            .map(\.id)
            .eraseToAnyPublisher()
    }
}
