// Copyright © Blockdaemon All rights reserved.

import CommonCryptoKit
import Foundation

struct SignedTransactionRequest: Encodable {
    let unsignedTransaction: String
    let signedInputs: [SignedInputs]
    let keyId: String
    let protocolId: String
}
