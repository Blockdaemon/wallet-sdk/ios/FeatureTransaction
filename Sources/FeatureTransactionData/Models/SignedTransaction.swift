// Copyright © Blockdaemon All rights reserved.

import Foundation

struct SignedTransaction: Encodable {
    let signedTransaction: String
    let protocolId: String
}
