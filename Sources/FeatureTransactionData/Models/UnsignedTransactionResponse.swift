// Copyright © Blockdaemon All rights reserved.

import CommonCryptoKit
import Foundation

struct UnsignedTransactionResponse<Intent: Decodable>: Decodable {
    let unsignedTransactionHash: String
    let intent: Intent
    let signingInputs: [SigningInput]
    
    enum CodingKeys: String, CodingKey {
        case unsignedTransactionHash = "unsignedTransaction"
        case intent
        case signingInputs
    }
}
