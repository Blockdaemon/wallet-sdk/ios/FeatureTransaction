// Copyright © Blockdaemon All rights reserved.

import Foundation

struct UnsignedTransactionRequest<E: Encodable>: Encodable {
    let protocolId: String
    let keyId: String
    let inputs: E
}
